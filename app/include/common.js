$(function() {

	// Мобильное меню
	$(".menu-toggle").click(function() {
		$('body').toggleClass("menu-expanded");
	});
	// аккордион в мобильном меню
	$('.menu-arrow').click(function () {
		if( $(window).width() < 1199 ) {
			if( $(this).hasClass('--active') ) {
				$(this).removeClass('--active');
				$(this).parent().find('.submenu').slideUp(200);
			} else {
				$(this).closest('.menu').find('.submenu').slideUp(200);
				$('.menu-arrow').removeClass('--active');
				$(this).addClass('--active');
				$(this).parent().find('.submenu').slideDown(200);
			}
		}
	});

	// попап гео
	$('.location__toggle').click(function () { 
		$(this).closest('.location').addClass('is-visible');
	});
	$('.location__link').click(function() {
		$(this).closest('.location__city').find('.location__toggle').text( $(this).text() );
		$(this).closest('.location').removeClass('is-visible');
	});

	// закрытие по клику за пределами
	$(document).mouseup(function (e){

		var interactiveElems = [ $('.location'), $('.f-menu') ],
			div;
		for(i=0; i<interactiveElems.length; ++i) {
			div = interactiveElems[i];
			if ( (!div.is(e.target) && div.has(e.target).length === 0) ) {
				if(div.hasClass('is-visible')) div.removeClass('is-visible');
			}
		}

	});


	$('.f-menu__btn').click(function () { 
		$('.f-menu').toggleClass('is-visible');
	});
	$('.f-menu__link').click(function () { 
		$('.f-menu').removeClass('is-visible');
	});





	// Jquery tabs
	$(".js-tab").click(function() {
		var hidden = $(this).data('hidden');
		$(this).closest('.js-tab-wrapper').find(".js-tab").removeClass("is-active")
		$(this).addClass("is-active");
		$(this).closest('.js-tab-wrapper').find(".js-tab-item").hide();
		$(hidden).fadeIn("normal");
	});



	// Слайдеры start

	// Слайдер "Что печатаем?" 
	var printSlider;
	function printSliderInit() {
		if ($(window).width() > 1199) {
			if ($('.hero-print__screen').hasClass('swiper-container-initialized')) {
				printSlider.destroy();
			}
		}
		else{
			if (!$('.hero-print__screen').hasClass('swiper-container-initialized')) {
				printSlider = new Swiper('.hero-print__screen', {
					slidesPerView: 'auto',
					spaceBetween: 0,
					initialSlide: 1,
					observer: true,
					observeParents: true,
					centeredSlides: true
				});
			}
		}
	}

	// Слайдер "На чём печатаем?" 
	var clothSlider = new Swiper('.cloth__slider', {
		loop: true,
		slidesPerView: 4,
		spaceBetween: 30,
		observer: true,
		observeParents: true,
		pagination: {
			el: '.cloth-dots',
		},
		breakpoints: {
			320: {
				slidesPerView: 1,
				spaceBetween: 0,
				centeredSlides: true,
			},
			576: {
				slidesPerView: 2,
				spaceBetween: 30
			},
			992: {
				slidesPerView: 3,
			},
			1200: {
				slidesPerView: 4,
				centeredSlides: false,
				navigation: {
					nextEl: '.cloth-next',
					prevEl: '.cloth-prev',
				},
			},
		}
	});

	// Слайдер "Отзывы" 
	var testimonialsSlider;
	function testimonialsSliderInit() {
		if ($(window).width() > 1199) {
			if ($('.testimonials__slider').hasClass('swiper-container-initialized')) {
				testimonialsSlider.destroy();
			}
		}
		else{
			if (!$('.testimonials__slider').hasClass('swiper-container-initialized')) {
				testimonialsSlider = new Swiper('.testimonials__slider', {
					slidesPerView: 1,
					spaceBetween: 0,
					initialSlide: 1,
					observer: true,
					observeParents: true,
					pagination: {
						el: '.testimonials-dots',
					},
				});
			}
		}
	}

	// Слайдер "Партнеры" 
	var partnersSlider = new Swiper('.partners__slider', {
		slidesPerView: 5,
		spaceBetween: 30,
		observer: true,
		observeParents: true,
		pagination: {
			el: '.partners-dots',
		},
		breakpoints: {
			320: {
				slidesPerView: 2,
				initialSlide: 1,
				spaceBetween: 30,
				centeredSlides: true,
				navigation: ''
			},
			576: {
				slidesPerView: 3,
				initialSlide: 2,
			},
			992: {
				slidesPerView: 4,
				centeredSlides: false,
			},
			1200: {
				slidesPerView: 5,
				navigation: {
					nextEl: '.partners-next',
					prevEl: '.partners-prev',
				}
			}
		}
	});

	// Слайдер "Скидки" 
	var discountsSlider = new Swiper('.discounts__slider', {
		slidesPerView: 1,
		spaceBetween: 0,
		observer: true,
		observeParents: true,
		pagination: {
			el: '.discounts-dots',
		},
	});

	// Слайдер "Рубрики блога" 
	var blogCataSlider = new Swiper('.blog-categories__slider', {
		loop: true,
		slidesPerView: 3,
		spaceBetween: 30,
		observer: true,
		observeParents: true,
		pagination: {
			el: '.blog-categories-dots',
		},
		breakpoints: {
			320: {
				slidesPerView: 1,
				spaceBetween: 0,
				navigation: '',
				centeredSlides: true,
			},
			576: {
				slidesPerView: 2,
				spaceBetween: 15
			},
			768: {
				slidesPerView: 2,
				spaceBetween: 30
			},
			1200: {
				slidesPerView: 3,
				centeredSlides: false,
				navigation: {
					nextEl: '.blog-categories-next',
					prevEl: '.blog-categories-prev',
				},
			},
		}
	});

	// Слайдер в статье 
	var articleSlider = new Swiper('.article-slider', {
		slidesPerView: 2,
		spaceBetween: 30,
		observer: true,
		observeParents: true,
		pagination: {
			el: '.article-slider-dots',
		},
		breakpoints: {
			320: {
				slidesPerView: 'auto',
				initialSlide: 1,
				spaceBetween: 5,
				navigation: '',
				centeredSlides: true,
			},
			992: {
				slidesPerView: 2,
				initialSlide: 0,
				spaceBetween: 30,
				centeredSlides: false,
				navigation: {
					nextEl: '.article-slider-next',
					prevEl: '.article-slider-prev',
				}
			}
		}
	});

	// Слайдер в табах 
	var tabsSlider;
	$('.tabs .tabs-nav__item').click(function () { 
		var hidden = $(this).data('hidden');
		tabsSliderInit(hidden);
	});
	
	function tabsSliderInit(hidden) {

		var currentSlider = hidden + ' .tabs-slider';

		if ($('.tabs-slider').hasClass('swiper-container-initialized')) {
			tabsSlider.destroy();
		}

		tabsSlider = new Swiper(currentSlider, {
			slidesPerView: 2,
			spaceBetween: 30,
			observer: true,
			observeParents: true,
			pagination: {
				el: '.tabs-dots',
			},
			breakpoints: {
				320: {
					slidesPerView: 'auto',
					initialSlide: 1,
					spaceBetween: 5,
					navigation: '',
					centeredSlides: true,
				},
				992: {
					slidesPerView: 2,
					initialSlide: 0,
					spaceBetween: 30,
					centeredSlides: false,
					navigation: {
						nextEl: '.tabs-next',
						prevEl: '.tabs-prev',
					}
				}
			}
		});

	}

	$(document).ready(function () {
		printSliderInit();
		testimonialsSliderInit();
		tabsSliderInit('.tabs-hiddden-1');
	});

	$(window).on('resize orientationchange', function() {
		printSliderInit();
		testimonialsSliderInit();
	});

	// Слайдеры end


	// Readmore
	$('.testimonials-more__btn').click(function () { 
		$(this).closest('.testimonials-more').find('.testimonials-more__txt').slideToggle(200);
	});

	$('.filter-btn').click(function () { 
		$('.testimonials-filters').toggleClass('is-visible');
	});

	
	$(".js-open-modal").on("click", function (e) {
		e.preventDefault();
		var modal = $(this).data('modal');
		$(modal).modal({
			showClose: false
		});
	});
	$(".modal .close").on("click", function (e) {
		$.modal.close();
	});


	// input type file
	$('.file').on('change', function() {
		var filesCount = +$(this)[0].files.length;
		var $textContainer = $(this).closest('.attach').find('.attach-msg');
		$textContainer.show();
		if (filesCount == 1) {
			var fileName = $(this).val().split('\\').pop();
			$textContainer.text(fileName);
		} else if (filesCount > 1 && filesCount < 5 ) {
			$textContainer.text(filesCount + ' файла выбрано');
		} else if ( filesCount == 0 || (filesCount > 5 && filesCount < 21) ) {
			$textContainer.text(filesCount + ' файлов выбрано');
		} else {
			$textContainer.text(filesCount + ' файла(ов) выбрано');
		}
	});



	Inputmask({
		mask:['+7 (999) 999-99-99'],
		greedy:false,
		showMaskOnHover:false
	}).mask(".js-tel-mask");


	// кнопка "Еще спецэфекты"
	var effectsToShow = 3;
	$('.effects__col').slice(0, effectsToShow).show();
	$('.effects .more-btn .button').click(function (e) { 
		e.preventDefault();
		effectsToShow += 3;
		$('.effects__col').slice(0, effectsToShow).show();
		if (effectsToShow >= $('.effects__col').length) $('.effects .more-btn .button').hide();
	});



});