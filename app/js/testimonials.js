// var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
// $('#reviews_content').on('click', '.show_more', function(){$(this).prev('div.text').html($(this).prev('div.text').children('div.more').html()); $(this).remove();});
// $('#reviews_content').on('click', '.filter .link', function(){
// 	var cls = $(this).attr('class').split(' ');
// 	if (cls[0] == 'indent') {
// 		if (cls[2] == 'checked') {
// 			$(this).removeClass('checked');
// 		} else {
// 			$(this).addClass('checked');
// 		}
// 	} else if (cls[0] == 'mood') {
// 		$('#reviews_content .filter .link.mood.checked').removeClass('checked');
// 		$('#reviews_content .filter .link.all').removeClass('checked');
// 		$(this).addClass('checked').children('div');
// 	} else if (cls[0] == 'all') {
// 		$('#reviews_content .filter .link.mood').each(function(){$(this).removeClass('checked');});
// 		$(this).addClass('checked');
// 	} else if (cls[0] == 'studio') {
// 		$('#reviews_content .filter .link.indent').each(function(){$(this).addClass('checked');});
// 	}
// 	param = {}
// 	param['setfilter'] = 'True';
// 	param['csrfmiddlewaretoken'] = csrftoken;
// 	i = 0;
// 	$('#reviews_content .filter .link.checked').each(function(){
// 		var clm = $(this).attr('class').split(' ');
// 		if (clm[0] == 'all' || clm[0] == 'mood') var key = 'mood';
// 		else {var key = 'studio_' + i; ++i;}
// 		param[key] = $(this).attr('href')
// 	});
// 	param['i'] = i;
// 	$('#reviews_content .wrap.justify-center').load('/reviews/', param);
// });




$(function() {


	$( 'input[name="review_date"]' ).datepicker({
		defaultDate: "0",
		changeMonth: true,
		dateFormat: 'yy-mm-dd',
		numberOfMonths: 1,
		currentText: 'Сегодня',
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь', 'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 'Июл','Авг','Сен','Окт','Ноя','Дек'],
		dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
		dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		firstDay: 1,
		maxDate: 0
	});


	$('.form-retail select[name="studio"]' ).change( function(e) {
		if( $(this).val() == 6 ) {
			$('.form-wholesale select[name="studio"] option').removeAttr('selected');
			$('.form-wholesale select[name="studio"] option:last-child').attr('selected', 'selected');
			$('.form-retail').hide();
			$('.form-wholesale').show();
		} 
	});
	$('.form-wholesale select[name="studio"]' ).change( function(e) {
		if( $(this).val() != 6 ) {
			$('.form-retail select[name="studio"]').val($(this).val());
			$('.form-wholesale').hide();
			$('.form-retail').show();
		} 
	});


});