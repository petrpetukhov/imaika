jQuery(document).ready(function ($) {


	let data_textile = ""; // выбранный текстиль
	let val_textile = ""; // выбранный текстиль
	let text_textile = ""; // выбранный текстиль
	let data_textileColor = ""; // выбранный тип цвета текстиля
	let val_textileColor = ""; // выбранный тип цвета текстиля
	let text_textileColor = ""; // выбранный тип цвета текстиля

	let data_textileColorShade = ""; // выбранный цвет текстиля


	let quantity_textile = 0; // кол-в выбранного текстиля
	let data_typePrint = ""; // Что будем печатать
	// let val_typePrint = ""; // Что будем печатать
	let text_typePrint = ""; // Что будем печатать


	let data_quantityColor = ""; // Кол-во цветов в макете
	let val_quantityColor = ""; // Кол-во цветов в макете
	let data_size = ""; // размер
	// let val_size = ""; // размер
	let text_size = ""; // размер


	let data_MethodPrint = ""; // Возможные варианты печати
	let val_MethodPrint = ""; // Возможные варианты печати

	let priceTextile = 0; // стоимость 1 единицы текстиля
	let total_priceTextile = 0; // всего за текстиль

	let priceMethodPrint = 0; // Итого за вариант печати 

	let price_thermalTransfer = 0; // стоимость за Термоперенос
	let total_price_thermalTransfer = 0; // всего за Термоперенос
	let price_directPrint = 0; // стоимость за Прямая печать
	let total_price_directPrint = 0; // всего за Прямая печать
	let price_silkscreen = 0; // стоимость за Шелкография
	let total_price_silkscreen = 0; // всего за Шелкография

	let total_price_calc = 0; // итого за заказ


	let columnPrice__textile = "";  // столбик в таблице Цены на чистый текстиль (без нанесения)
	let columnPrice__method_thermalTransfer = "";  // столбик в таблице термоперенос
	let columnPrice__method_directPrint = "";  // столбик в таблице прямая печать
	let columnPrice__method_silkscreen = "";  // столбик в таблице шелкография

	let textEnding = "";  // составляющая data attr для выбора слова
	let lastNumber = 0; // последняя цифра в значении






	// стилизация input type number
	function quantityProducts() {
		// console.log("стилизация кнопок input ");
		var $quantityArrowMinus = $(".calc__quantity_arrow_minus");
		var $quantityArrowPlus = $(".calc__quantity_arrow_plus");
		var $quantityNum = "";
		$quantityArrowMinus.click(quantityMinus);
		$quantityArrowPlus.click(quantityPlus);
		function quantityMinus() {
			$quantityNum = $(this).closest(".calc__quantity_block").find(".calc__InputQuantity_js");
			if ($quantityNum.val() > 1) {
				$quantityNum.val(+$quantityNum.val() - 1);
				// console.log($quantityNum);
			}
			calcParams();
		};
		function quantityPlus() {
			$quantityNum = $(this).closest(".calc__quantity_block").find(".calc__InputQuantity_js");
			$quantityNum.val(+$quantityNum.val() + 1);
			calcParams();
		};
	};
	quantityProducts();
	// стилизация input type number


	$(".input_changeCalc_js").on("change", function () {
		calcParams();
	});

	$("#input_quantityColor").on("change keyup", function () {
		calcParams();
	});











	// основной расчет
	function calcParams() {
		// console.log(" Делаем расчет ");

		// получение всех параметров

		data_textile = $("input[name='tshortswitch']:checked").attr("data-param");
		// console.log(data_textile);
		val_textile = $("input[name='tshortswitch']:checked").val();

		data_textileColor = $("input[name='colorswitch']:checked").attr("data-param");
		// console.log(data_textileColor);

		val_textileColor = $("input[name='colorswitch']:checked").val();
		// console.log(val_textileColor);

		data_textileColorShade = $("input[name='colorswitch']:checked").attr("data-paramShade");
		// console.log(data_textileColorShade);

		quantity_textile = +$("#input_quantityColor").val();
		// console.log(quantity_textile);

		data_typePrint = $("input[name='printtypeswitch']:checked").attr("data-param");
		// console.log(data_typePrint);

		text_typePrint = $("input[name='printtypeswitch']:checked").attr("data-textTypePrint");
		// console.log(text_typePrint);

		data_quantityColor = $("input[name='colorrangeswitch']:checked").attr("data-param");
		// console.log(data_quantityColor);

		val_quantityColor = $("input[name='colorrangeswitch']:checked").val();
		// console.log(val_quantityColor);

		data_size = $("input[name='printsizeswitch']:checked").attr("data-param");
		// console.log(data_size);

		text_size = $("input[name='printsizeswitch']:checked").attr("data-textTypeSize");
		// console.log(data_size);

		data_MethodPrint = $("input[name='printmethodswitch']:checked").attr("data-param");
		// console.log(data_MethodPrint);

		val_MethodPrint = $("input[name='printmethodswitch']:checked").val();
		// console.log(val_MethodPrint);

		// получение всех параметров





		// текстиль

		$("#tableCalcPrice__textile").find("[data-quantity]").each(function (index, element) {
			// console.log(quantity_textile);
			// console.log(+$(this).attr("data-quantity"));
			if (quantity_textile < +$(this).attr("data-quantity")) {
				// console.log(+$(this).attr("data-quantity"));
				// console.log(index);
				columnPrice__textile = index;
				return false;
			} else {
				columnPrice__textile = $("#tableCalcPrice__textile").find("[data-quantity]").length;
				// console.log(" Зашли в проверку, проверка не выполнилась ");
				// console.log(columnPrice__textile);
			};
		});

		priceTextile = +$("#tableCalcPrice__textile").find(`[data-price='${data_textile}` + '_' + `${data_textileColor}']`).children("td").eq(columnPrice__textile + 1).text();
		total_priceTextile = priceTextile * quantity_textile;
		// priceTextile = $("#tableCalcPrice__textile").find("[data-price='shirt_color']").children("td").eq(4).text();
		// console.log(priceTextile);

		// текстиль








		// термоперенос

		$("#tableCalcPrice__method_thermalTransfer").find("[data-quantity]").each(function (index, element) {
			// console.log(quantity_textile);
			// console.log(+$(this).attr("data-quantity"));
			if (quantity_textile < +$(this).attr("data-quantity")) {
				// console.log(" Зашли в проверку ");
				// console.log(+$(this).attr("data-quantity"));
				// console.log(index);
				columnPrice__method_thermalTransfer = index;
				// console.log(columnPrice__method_thermalTransfer);
				return false;
			}
			else {
				columnPrice__method_thermalTransfer = $("#tableCalcPrice__method_thermalTransfer").find("[data-quantity]").length;
				// console.log(" Зашли в проверку, проверка не выполнилась ");
				// console.log(columnPrice__method_thermalTransfer);
			};
		});


		price_thermalTransfer = +$("#tableCalcPrice__method_thermalTransfer").find(`[data-price='${data_size}` + '_' + `${data_quantityColor}']`).children("td").eq(columnPrice__method_thermalTransfer + 1).text();
		// console.log(price_thermalTransfer);

		// если нет значения или выбрана "Фото/иллюстрацию"
		if (price_thermalTransfer == "" || data_typePrint == "photo") {
			price_thermalTransfer = 0;
			$("input[data-param='thermalTransfer']").next("label").addClass("deactivate");
			$("input[data-param='thermalTransfer']").attr("checked", false).attr("disabled", true);
			$("input[data-param='thermalTransfer']").prop("checked", false).prop("disabled", true);
		} else {
			$("input[data-param='thermalTransfer']").next("label").removeClass("deactivate");
			$("input[data-param='thermalTransfer']").attr("disabled", false);
			$("input[data-param='thermalTransfer']").prop("disabled", false);

		};
		// console.log(price_thermalTransfer);
		total_price_thermalTransfer = price_thermalTransfer * quantity_textile;
		// console.log(total_price_thermalTransfer);


		// заносим цену метода печати
		$("input[data-param='thermalTransfer']").attr('data-total', total_price_thermalTransfer);

		// термоперенос






		// прямая печать

		$("#tableCalcPrice__method_directPrint").find("[data-quantity]").each(function (index, element) {
			// console.log(quantity_textile);
			// console.log(+$(this).attr("data-quantity"));
			if (quantity_textile < +$(this).attr("data-quantity")) {
				// console.log(+$(this).attr("data-quantity"));
				// console.log(index);
				columnPrice__method_directPrint = index;
				return false;
			} else {
				columnPrice__method_directPrint = $("#tableCalcPrice__method_directPrint").find("[data-quantity]").length;
				// console.log(" Зашли в проверку, проверка не выполнилась ");
				// console.log(columnPrice__method_directPrint);
			};
		});

		price_directPrint = +$("#tableCalcPrice__method_directPrint").find(`[data-price='${data_size}` + '_' + `${data_textileColor}']`).children("td").eq(columnPrice__method_directPrint + 1).text();
		// console.log(price_directPrint);
		total_price_directPrint = price_directPrint * quantity_textile;
		// console.log(total_price_directPrint);

		// заносим цену метода печати
		$("input[data-param='directPrint']").attr('data-total', total_price_directPrint);

		// прямая печать







		// шелкография

		$("#tableCalcPrice__method_silkscreen").find("[data-quantity]").each(function (index, element) {
			// console.log(quantity_textile);
			// console.log(+$(this).attr("data-quantity"));
			if (quantity_textile < +$(this).attr("data-quantity")) {
				// console.log(+$(this).attr("data-quantity"));
				// console.log(index);
				columnPrice__method_silkscreen = index;
				// console.log(columnPrice__method_silkscreen);
				return false;
			}
			else {
				// console.log(" Зашли в проверку, проверка не выполнилась ");
				columnPrice__method_silkscreen = $("#tableCalcPrice__method_silkscreen").find("[data-quantity]").length;
				// console.log(columnPrice__method_silkscreen);
			};
		});

		price_silkscreen = +$("#tableCalcPrice__method_silkscreen").find(`[data-price='${data_size}` + '_' + `${data_quantityColor}']`).children("td").eq(columnPrice__method_silkscreen + 1).text();
		// console.log(price_silkscreen);


		// console.log(+$("#tableCalcPrice__method_silkscreen").find("[data-quantity]").eq(0).attr("data-quantity") );
		// если нет значения или выбрана

		if (price_silkscreen == "" || quantity_textile < +$("#tableCalcPrice__method_silkscreen").find("[data-quantity]").eq(0).attr("data-quantity")) {
			price_silkscreen = 0;
			$("input[data-param='silkscreen']").next("label").addClass("deactivate");
			$("input[data-param='silkscreen']").attr("checked", false).attr("disabled", true);
			$("input[data-param='silkscreen']").prop("checked", false).prop("disabled", true);
			// console.log(" скрываем ");
		} else {
			$("input[data-param='silkscreen']").next("label").removeClass("deactivate");
			$("input[data-param='silkscreen']").attr("disabled", false);
			$("input[data-param='silkscreen']").prop("disabled", false);
			// console.log(" показываем ");

		};


		total_price_silkscreen = price_silkscreen * quantity_textile;
		// console.log(total_price_silkscreen);

		// заносим цену метода печати
		$("input[data-param='silkscreen']").attr('data-total', total_price_silkscreen);

		// шелкография





		// стоиомть вариант печати
		priceMethodPrint = +$("input[name='printmethodswitch']:checked").attr("data-total");
		// console.log(priceMethodPrint);

		// полная стоимость
		total_price_calc = total_priceTextile + priceMethodPrint;
		// console.log(total_price_calc);






		// проверка окончаний

		lastNumber = quantity_textile % 10; // последняя цифра в значении
		// console.log(lastNumber);

		textEnding = "text3"; // устанавливаем частое значение

		// если 1 или 21,31,41 и т.д.
		if (quantity_textile == 1 || lastNumber == 1) {
			textEnding = "text1";
		};
		// если 22,23,24, 32,33,34 и т.д.
		if (lastNumber == 2 || lastNumber == 3 || lastNumber == 4) {
			textEnding = "text2";
		};
		// если от 5 до 20 включительно
		if (quantity_textile >= 5 && quantity_textile <= 20) {
			textEnding = "text3";
		};

		// console.log(textEnding);
		// console.log(data_textile);


		// название текстиля с нужным окончанием
		text_textile = $(`input[data-${data_textile}` + "_" + `${textEnding}]`).attr(`data-${data_textile}` + "_" + `${textEnding}`);
		// console.log(text_textile);

		// название цвета с нужным окончанием
		// console.log(data_textileColorShade);
		text_textileColor = $(`input[data-color_${data_textileColorShade}` + "_" + `${data_textile}` + "_" + `${textEnding}]`).attr(`data-color_${data_textileColorShade}` + "_" + `${data_textile}` + "_" + `${textEnding}`);
		// console.log(text_textileColor);


		// проверка окончаний



		// вывод текста на странице
		$("#priceTextile").text(total_priceTextile.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
		$("#price__thermalTransfer").text(total_price_thermalTransfer.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));  		// термоперенос
		$("#price__directPrint").text(total_price_directPrint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")); // прямая печать
		$("#price__silkscreen").text(total_price_silkscreen.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")); // прямая печать

		$(".c-quantity").text(quantity_textile);
		$(".c-color").text(text_textileColor);
		$(".c-shirt").text(text_textile);
		$(".c-item").text(text_typePrint);
		$(".c-size").text(text_size);
		// вывод текста на странице



		// кнопка вызлва модального окна
		if (
			+$("input[name='tshortswitch']:checked").length == 1 &&
			+$("input[name='colorswitch']:checked").length == 1 &&
			+$("input[name='printtypeswitch']:checked").length == 1 &&
			+$("input[name='colorrangeswitch']:checked").length == 1 &&
			+$("input[name='printsizeswitch']:checked").length == 1 &&
			+$("input[name='printmethodswitch']:checked").length == 1
		) {
			$("#calc__buttonFormShow").attr("disabled", false)
		}
		else {
			$("#calc__buttonFormShow").attr("disabled", true)
		}
		// кнопка вызлва модального окна





		// передача нужной информации в форму обратной связи модальное окно


		$("#calc__buttonFormShow").on("click", function (e) {
			e.preventDefault();
			// console.log(" открываем форму ");

			// заголовки
			$("#calc-form__text_textile").text(val_textile);
			$("#calc-form__text_color").text(val_textileColor);
			$("#calc-form__text_quantity").text(quantity_textile);
			$("#calc-form__priceTextile").text(total_priceTextile.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			$("#calc-form__typePrint").text(text_typePrint);
			$("#calc-form__quantityColor").text(val_quantityColor);
			$("#calc-form__size").text(text_size);
			$("#calc-form__methodPrint").text(val_MethodPrint);
			$("#calc-form__priceMethodPrint").text(priceMethodPrint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			$("#calc-form__totalPrice").text(total_price_calc.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));

			// скрытые поля
			$("#input_calc-form__text_textile").val(val_textile);
			$("#input_calc-form__text_color").val(val_textileColor);
			$("#input_calc-form__text_quantity").val(quantity_textile);
			$("#input_calc-form__priceTextile").val(total_priceTextile.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			$("#input_calc-form__typePrint").val(text_typePrint);
			$("#input_calc-form__quantityColor").val(val_quantityColor);
			$("#input_calc-form__size").val(text_size);
			$("#input_calc-form__methodPrint").val(val_MethodPrint);
			$("#input_calc-form__priceMethodPrint").val(priceMethodPrint.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			$("#input_calc-form__totalPrice").val(total_price_calc.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));

		});




	};










}); //end function









function hide() {


	var pld = 1,
		cnt = 0,
		send = !1,
		sclick = !0,
		csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
	function sendtex() {
		var error = !1,
			param = {},
			print = !1;
		(param.cv = $("#calculator_range .value").val()),
			(null != param.cv && "" != param.cv && "0" != param.cv) || (error = !0),
			(param.tx = $('input[name="tshortswitch"]:checked').val()),
			(null != param.tx && "" != param.tx) || (error = !0),
			(param.tc = $('input[name="colorswitch"]:checked').val()),
			(null != param.tc && "" != param.tc) || (error = !0),
			error
				? ($(".cutline .title .value").html("0"),
					$("#calculator #aboutprints label").each(function () {
						$(this).addClass("deactivate");
					}),
					$("#calculator #service label").each(function () {
						$(this).addClass("deactivate"), $(this).children("div").children(".price").remove();
					}),
					$("#calculator .total.right span").html("- "))
				: ((param.csrfmiddlewaretoken = csrftoken),
					(param.calc = "textile"),
					$.post("https://www.i-maika.ru/calculate/", param, function (res) {
						(res = parseInt(res)),
							$(".cutline .title .value").html(res + " "),
							0 != res
								? $("#calculator #aboutprints label").each(function () {
									$(this).removeClass("deactivate");
								})
								: ($(".cutline .title .value").html("0"),
									$("#calculator #aboutprints label").each(function () {
										$(this).addClass("deactivate");
									}),
									$("#calculator #service label").each(function () {
										$(this).addClass("deactivate");
									}),
									$("#calculator .total.right span").html("- "));
					}),
					sendprint());
	}
	function sendprint() {
		var error = !1,
			param = {};
		(param.cv = $("#calculator_range .value").val()),
			(null != param.cv && "" != param.cv && "0" != param.cv) || (error = !0),
			(param.tc = $('input[name="colorswitch"]:checked').val()),
			(null != param.tc && "" != param.tc) || (error = !0),
			(param.ps = $('input[name="printsizeswitch"]:checked').val()),
			(null != param.ps && "" != param.ps) || (error = !0),
			(param.pt = $('input[name="printtypeswitch"]:checked').val()),
			(null != param.pt && "" != param.pt) || (error = !0),
			(param.cr = $('input[name="colorrangeswitch"]:checked').val()),
			(null != param.cr && "" != param.cr) || (error = !0),
			error || ((param.csrfmiddlewaretoken = csrftoken), (param.calc = "print"), $("#service .elem").load("/calculate/", param)),
			$("#calculator .total.right span").html("- ");
	}
	$("#calculator_range .minus")
		.on("mousedown", function () {
			clearInterval($("#calculator_range .value").data("chval")),
				$("#calculator_range .value").data(
					"chval",
					setInterval(function () {
						(cnt += 1) % 10 == 0 && (pld *= 10);
						var cv = $("#calculator_range .value").val();
						cv != parseInt(cv) || cv <= pld ? (cv = 0) : 1 != cnt && ((cv = parseInt(cv) - pld), (send = !0), (sclick = !1)), $("#calculator_range .value").val(cv);
					}, 100)
				);
		})
		.on("mouseup", function () {
			(cnt = 0), (pld = 1), clearInterval($("#calculator_range .value").data("chval")), send && sendtex(), (send = !1);
		})
		.on("mouseout", function () {
			(cnt = 0), (pld = 1), clearInterval($("#calculator_range .value").data("chval")), send && sendtex(), (send = !1);
		})
		.on("click", function () {
			var cv = $("#calculator_range .value").val();
			(cv = cv != parseInt(cv) || cv <= 1 ? 0 : parseInt(cv) - 1), $("#calculator_range .value").val(cv), sclick ? sendtex() : (sclick = !0);
		}),
		$("#calculator_range .plus")
			.on("mousedown", function () {
				clearInterval($("#calculator_range .value").data("chval")),
					$("#calculator_range .value").data(
						"chval",
						setInterval(function () {
							(cnt += 1) % 10 == 0 && (pld *= 10);
							var cv = $("#calculator_range .value").val();
							cv != parseInt(cv) ? (cv = 0) : 1 != cnt && ((cv = parseInt(cv) + pld), (send = !0), (sclick = !1)), $("#calculator_range .value").val(cv);
						}, 100)
					);
			})
			.on("mouseup", function () {
				(cnt = 0), (pld = 1), clearInterval($("#calculator_range .value").data("chval")), send && sendtex(), (send = !1);
			})
			.on("mouseout", function () {
				(cnt = 0), (pld = 1), clearInterval($("#calculator_range .value").data("chval")), send && sendtex(), (send = !1);
			})
			.on("click", function () {
				var cv = $("#calculator_range .value").val();
				(cv = cv != parseInt(cv) ? 1 : parseInt(cv) + 1), $("#calculator_range .value").val(cv), sclick ? sendtex() : (sclick = !0);
			}),
		$("#calculator_range .value").on("keyup", function () {
			var cv = parseInt($("#calculator_range .value").val());
			cv >= 0 ? $("#calculator_range .value").val(cv) : $("#calculator_range .value").val(""), sendtex();
		}),
		$('input[name="tshortswitch"]').on("change, click", function () {
			sendtex();
		}),
		$('input[name="colorswitch"]').on("change, click", function () {
			sendtex();
		}),
		$('input[name="printsizeswitch"]').on("change, click", function () {
			sendprint();
		}),
		$('input[name="printtypeswitch"]').on("change, click", function () {
			sendprint();
		}),
		$('input[name="colorrangeswitch"]').on("change, click", function () {
			sendprint();
		}),
		$("#calculator #service").on("click", "label", function () {
			var cls;
			if ("deactivate" != $(this).attr("class")) {
				var tp = parseInt($(".cutline .title .value").html()),
					sp = parseInt($(this).children("div").children(".price").children("span").html());
				$("#calculator .total.right span").html(tp + sp + " "), $("#calculator .total.right").show();
			}
		}),
		$("#calculator").on("click", "label.deactivate", function () {
			return !1;
		});
	// $(document).on("click", ".optPriceDnld", function () {
	//     return ym(461011, "reachGoal", "optPrice"), !0;
	// });


};